/*
 * fat_on_sd.h
 *
 *  Created on: 1 juin 2018
 *      Author: hanch_000
 */

#ifndef FAT_ON_SD_H_
#define FAT_ON_SD_H_

#include "fatfs/fatfs_include/ff.h"

int FatOnSd_Init();

#endif /* FAT_ON_SD_H_ */

/*
 * fat_on_sd.c
 *
 *  Created on: 1 juin 2018
 *      Author: hanch_000
 */


#include "fat_on_sd.h"
#include "sdmmc/inc/fsl_sd.h"
#include "fatfs/fatfs_include/ff.h"
#include "fatfs/fatfs_include/diskio.h"
#include "fatfs/fatfs_include/fsl_sd_disk.h"
#include "ffconf.h"
#include "fsl_debug_console.h"
#include "board.h"

/*
 * Static variables
 */

static FATFS g_fileSystem; /* File system object */

static const sdmmchost_detect_card_t s_sdCardDetect = {
    .cdType = kSDMMCHOST_DetectCardByHostDATA3,
    .cdTimeOut_ms = (~0U),
};

/*
 * Static functions
 */

static status_t sdcardWaitCardInsert(void);


/*
 * Public functions
 */

status_t FatOnSd_Init() {

	const TCHAR driverNumberBuffer[3U] = { SDDISK + '0', ':', '/' };
	BYTE work[FF_MAX_SS];
	NVIC_SetPriority(BOARD_SDHC_IRQ, 5U);

	if (sdcardWaitCardInsert() != kStatus_Success) {
		return kStatus_Fail;
	}
	if (f_mount(&g_fileSystem, driverNumberBuffer, 0U)) {
		return kStatus_Fail;
	}
#if (FF_FS_RPATH >= 2U)
	if (f_chdrive((char const *) &driverNumberBuffer[0U])) {
		return kStatus_Fail;
	}
#endif
#if FF_USE_MKFS
	if (f_mkfs(driverNumberBuffer, FM_FAT32, 0U, work, sizeof work)) {
		return kStatus_Fail;
	}
#endif /* FF_USE_MKFS */
	return kStatus_Success;
}

/*
 * Static functions declarations
 */

static status_t sdcardWaitCardInsert(void) {
    /* Save host information. */
    g_sd.host.base = SD_HOST_BASEADDR;
    g_sd.host.sourceClock_Hz = SD_HOST_CLK_FREQ;
    /* card detect type */
    g_sd.usrParam.cd = &s_sdCardDetect;
#if defined DEMO_SDCARD_POWER_CTRL_FUNCTION_EXIST
    g_sd.usrParam.pwr = &s_sdCardPwrCtrl;
#endif
    /* SD host init function */
    if (SD_HostInit(&g_sd) != kStatus_Success) {
        return kStatus_Fail;
    }
    /* power off card */
    SD_PowerOffCard(g_sd.host.base, g_sd.usrParam.pwr);
    /* wait card insert */
    if (SD_WaitCardDetectStatus(SD_HOST_BASEADDR, &s_sdCardDetect, true) == kStatus_Success) {
        /* power on the card */
        SD_PowerOnCard(g_sd.host.base, g_sd.usrParam.pwr);
    } else {
        return kStatus_Fail;
    }
    return SD_CardInit(&g_sd);
}
